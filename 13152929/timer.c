#include "timer.h"

unsigned int SysTimer_ = 0;	    // 0.2ms计时器，脉冲输出功能
unsigned int SysTimer = 0;      // 1ms计时器，用于数码管刷新、定时任务
unsigned int SysTimer_led = 0;  // 100ms计时器，用于LED闪烁功能

bit Timer_10ms = 0;// 10ms标志位，用于按键扫描
bit Timer_300ms = 0;// 300ms标志位，用于ADC读取之类的
bit Timer_500ms = 0;// 500ms标志位，用于超声波测距
bit Timer_50ms = 0;// 50ms标志位，用于LED状态的刷新，省得一直刷新

bit dis_flag = 0;   // 继电器标志位【1-继电器吸合】【0-继电器断开】

unsigned char Wave_duty = 0;	// 脉冲输出功能：占空比

unsigned int Fre = 0;   // 频率测量：频率

unsigned char p0data=0;     // 脉冲输出功能
unsigned char p0data_=0xFF; // 脉冲输出功能


// 数码管显示部分
unsigned char SegBuf[8] = {10, 11,12,13,14,15,16,17};   // 对应8位数码管
    // 显示码表
unsigned char code SegList[] = {
    0xc0, 0xf9, 0xa4, 0xb0, 0x99, 0x92, 0x82, 0xf8, 0x80, 0x90,                                                             
    0xFF,0x8E,0x89,0x88,0x8C,
    //   11 F 12H  13A  14P
    0xc0 & 0x7F, 0xf9 & 0x7F, 0xa4 & 0x7F, 0xb0 & 0x7F, 0x99 & 0x7F, 0x92 & 0x7F, 0x82 & 0x7F, 0xf8 & 0x7F, 0x80 & 0x7F, 0x90 & 0x7F
};

// T0 测频,P34
// T1 定时
// t2 超声波
void Timer1_isr(void) interrupt 3
{
    // 0.2ms计时器，到一万清零省事儿
    SysTimer_ ++; 
    if(SysTimer_ > 10000) SysTimer_ = 0;
    
    
    if(dis_flag)
    {
        p0data |= 0x10;
    }
    else
    {
        p0data &= 0xEF;
    }
    
    // 每5个0.2ms就是1ms，对应的频率就是1kHz，也就是脉冲输出功能需要的信号的频率
    if(SysTimer_ % 5 == 0)  // 一个周期的起始
    {   
        p0data &= 0xDF; //  1101 1111 即让【N_MOTOR】输出高电平

        SysTimer++; // 1ms计时器，到六万清零儿
        if (SysTimer >= 60000)  SysTimer = 0; 
    
        // 每间隔1000ms测量频率
        if(SysTimer % 1000 == 0)
        {
            TR0 = 1;		//定时器0开始计时
        }
        else if(SysTimer % 1000 == 100) // 经过了100m的测量，计算频率
        {
            TR0 = 0;
            Fre = (TH0 << 8) + TL0;
            Fre *= 10;
            TL0 = 0;		//设置定时初始值
            TH0 = 0;		//设置定时初始值
            TF0 = 0;		//清除TF0标志
        }
            
        // 标志位
        if ((SysTimer % 10) == 0)
        {
            Timer_10ms = 1; // 10ms标志位，用于按键扫描
            if((SysTimer % 50) == 0)
            {
                Timer_50ms = 1; // 50ms标志位，用于LED状态的刷新，省得一直刷新
                if(SysTimer % 100 == 0)
                {
                    SysTimer_led ++;    // 100ms计时器，用于LED闪烁【灯闪烁的要求，这个方式我用着很简单】
                    
                    if(SysTimer % 300 == 0)
                    {
                        Timer_300ms = 1; // 300ms标志位，用于ADC读取之类的
                    }
                    if(SysTimer % 500 == 0)
                    {
                        Timer_500ms = 1; // 500ms标志位，用于超声波测距
                        
                    }
                }
            }
        }
        // 刷新数码管
        SegRefresh();
    }
    else if(SysTimer_ % 5 == Wave_duty)
    {
        p0data |= 0x20; 
    }
    
    if(p0data_ != p0data)
    {
        P0 = p0data;
        P2 = (P2 & 0x1F) | 0xA0;
        P2 &= 0x1F;
        p0data_ = p0data;
    }
}

// 定时器0，用于超声波测距计时
void Timer0Init_FRE(void)		//100微秒@12.000MHz
{
	AUXR |= 0x80;		//定时器时钟1T模式
	TMOD &= 0xF0;		//设置定时器模式
	TMOD |= 0x04;		//设置定时器模式
	TL0 = 0;		//设置定时初始值
	TH0 = 0;		//设置定时初始值
	TF0 = 0;		//清除TF0标志
	TR0 = 0;		//定时器0开始计时
    ET0 = 0;
}

// 本来是1ms中断一次，考虑到脉冲输出功能，改为0.2ms中断一次
void Timer1Init(void) // 0.2毫秒@12.000MHz
{
    AUXR |= 0x40; //定时器时钟1T模式
    TMOD &= 0x0F; //设置定时器模式
	TL1 = 0xA0;		//设置定时初始值
	TH1 = 0xF6;		//设置定时初始值
    TF1 = 0;      //清除TF1标志
    TR1 = 1;      //定时器1开始计时
    ET1 = 1;
    EA = 1;
}

// 数码管刷新显示函数，
void SegRefresh(void)
{
    static unsigned char SegCnt = 0;    // 静态变量，刷新1-8之间的任意一位数码管
    
    P0 = 0xFF;
    P2 = (P2 & 0x1F) | 0xE0; // 段
    P2 = (P2 & 0x1F);
    
    P0 = 1 << SegCnt;
    P2 = (P2 & 0x1F) | 0xC0; // 位
    P2 = (P2 & 0x1F);

    P0 = SegList[SegBuf[SegCnt]];
    P2 = (P2 & 0x1F) | 0xE0; // 段
    P2 = (P2 & 0x1F);

    SegCnt = ((SegCnt >= 7) ? 0 : (SegCnt + 1));    // 移位
}

// 按键扫描
// 按下返回：按键序号 | 0x20
// 长按返回：按键序号 | 0x40
// 松手返回：按键序号 | 0x80
unsigned char BTN_KeyScan(void)
{
	static unsigned char KeyPressBTN = 0;					 
	static unsigned char KeyFreeBTN = 0;
	static unsigned char KeyValueTemBTN = 0;				
	static unsigned char IntervalTimeBTN = 6; 
	unsigned char KeyValue = 0;								
	unsigned char tem;										

	P3 |= 0x0F;
	tem = (P3 & 0x0F);

	if (tem != 0x0F)
	{
		if (KeyPressBTN < 220)
			KeyPressBTN++;
	}
	else
	{
		KeyPressBTN = 0;
	}

	if (KeyFreeBTN == 0)
	{
		if (KeyPressBTN >= 5)
		{
			KeyFreeBTN = 1; // 按键按下
			IntervalTimeBTN = 200;

			switch (tem)
			{
			case 0x0E:
				KeyValueTemBTN = 7;
				break;
			case 0x0D:
				KeyValueTemBTN = 6;
				break;
			case 0x0B:
				KeyValueTemBTN = 5;
				break;
			case 0x07:
				KeyValueTemBTN = 4;
				break;
			default:
				break;
			}
		}
	}

	if (KeyFreeBTN == 1)
	{
		if (tem != 0x0F) // 按键未松开
		{
			if (KeyPressBTN == 5) // 第一次按下
			{
				KeyValue = KeyValueTemBTN | 0X20;
			}
			else if (KeyPressBTN >= IntervalTimeBTN) // 长按
			{
				KeyValue = KeyValueTemBTN | 0x40;
                IntervalTimeBTN = 0xFF;
			}
			else
			{
				KeyValue = 0;
			}
		}
		else // tem == 0xFF 按键松开
		{
			KeyFreeBTN = 0;
			KeyPressBTN = 0;
            if(IntervalTimeBTN == 0xFF)
            {
                KeyValue = KeyValueTemBTN | 0xC0;
            }
            else
            {
                KeyValue = KeyValueTemBTN | 0x80;
			}
            KeyValueTemBTN = 0;
		}
	}
	else
	{
		KeyValue = 0;
	}

	return KeyValue;
}

