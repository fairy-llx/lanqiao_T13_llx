#ifndef _IIC_H
#define _IIC_H

#include "stc15f2k60s2.h"
#include "intrins.h"

sbit SDA = P2^1;
sbit SCL = P2^0;

void IIC_Start(void); 
void IIC_Stop(void);  
bit IIC_WaitAck(void);  
void IIC_SendAck(bit ackbit); 
void IIC_SendByte(unsigned char byt); 
unsigned char IIC_RecByte(void); 

void DAC_PCF8591(unsigned char DACdata);
unsigned char EEPROM_Read(unsigned char Add);
//unsigned char ADC_PCF8591(unsigned char channel);
void EEPROM_Write(unsigned char Add, unsigned char _Data) ;

#endif