#include "iic.h"
#include "timer.h"

unsigned int distance; 
sbit csb_TX = P1^0;	
sbit csb_RX = P1^1;
bit csb_error=0;

void delay_ms(unsigned int ms);
void csb_GetDis(void);
void Timer2Init_csb(void);
void scb_Init(void) ;

void main(void)
{
    unsigned char Interface_main = 0;   // 主界面
    unsigned char Interface_p = 0;      // 参数界面
    
    unsigned char KeyValue = 0; // 按键的键值
    
    unsigned char ADC_Wet;      // ADC读取回来的湿度
    
    unsigned char P_fre = 90;   // 频率参数 9.0k    
    unsigned char P_wet = 40;   // wet      40%
    unsigned char P_dis = 6;    // dis      0.6m-60cm
    
    unsigned char DAC_out;      // DACS输出功能，
    
    unsigned char LEDstate;     //  LED灯的状态
    
    unsigned char Relay_cnt;    // 继电器开关次数，存储于EEPROM中
    
    unsigned char clear=0;      // 
    
    bit bit_fre = 0;            //  频率显示单位切换【1-kHz】【0-Hz】
    bit bit_dis = 0;            //  距离显示单位切换【1-m】【0-cm】

    // LED 【初始化，关灯】
    P0 = 0xFF;
	P2 = (P2 & 0x1F) | 0x80;
	P2 &= 0x1F;
    
    // 外设 【初始化，关外设】
    P0 = 0;
	P2 = (P2 & 0x1F) | 0xA0;
	P2 &= 0x1F;
    
    // 加载EEPROM中的继电器开关次数，读两次？稳妥！
    Relay_cnt = EEPROM_Read(0x00);
    Relay_cnt = EEPROM_Read(0x00);

    
    Timer0Init_FRE();   // 定时器0初始化，用于频率测量
    Timer1Init();       // 定时器1初始化，用于系统计时
    Timer2Init_csb();   // 定时器2初始化，用于超声波测距的计时
    
    while(1)
    {
        if(Timer_10ms)
        {
            Timer_10ms = 0;
            KeyValue = BTN_KeyScan();   // 按键扫描
            if(KeyValue)
            {
                switch(KeyValue)
                {
                    case 4 | 0x20:// 界面切换
                        Interface_main = ( (Interface_main>=3) ? 0 :(Interface_main+1) );
                        if(Interface_main == 3)Interface_p = 0;
                    break;
                    
                    case 5 | 0x20:// 参数切换
                        if(Interface_main == 3)
                        Interface_p = ( (Interface_p>=2)?  0 :(Interface_p+1) );
                    break;
                    
                    case 6 | 0x20:// 加
                        if(Interface_main == 3)
                        {
                            if(Interface_p == 0)
                            {
                                P_fre = ( (P_fre >= 120) ? 10 : (P_fre+5) );
                            }
                            else if(Interface_p == 1)
                            {
                                P_wet = ( (P_wet >= 60) ? 10 : (P_wet+10) );
                            }
                            else if(Interface_p == 2)
                            {
                                P_dis = ( (P_dis >= 12) ? 1 : (P_dis+1) );
                            }
                        } 
                        else if(Interface_main == 2)
                        {
                            bit_dis = !bit_dis; // cm - m
                        }   
                    break;
                    
                    case 7 | 0x20:// 减
                        if(Interface_main == 3)
                        {
                            if(Interface_p == 0)
                            {
                                  P_fre = ( (P_fre <= 10) ? 120 : (P_fre-5) );
                            }
                            else if(Interface_p == 1)
                            {
                                  P_wet = ( (P_wet <= 10) ? 60 : (P_wet-10) );
                            }
                            else if(Interface_p == 2)
                            {
                                  P_dis = ( (P_dis <= 1) ? 12 : (P_dis-1) );
                            }
                        } 
                        else if(Interface_main == 0)
                        {
                            bit_fre = !bit_fre; // hz-khz
                        }
                    break;
                    
                    case 7 | 0xC0:// 长按后松开【特意改的长按松开】
                        if(Interface_main == 1)
                        {
                            EA = 0;
                            EEPROM_Write(0x00, 0x00);
                            EA = 1;
                            Relay_cnt = 0;
                        }
                            
                    break;
                    
                    default:break;
                }
            }
            
            switch(Interface_main)
            {
                // 【频率界面】
                case 0: // fre
                    SegBuf[0] = 11;
                    SegBuf[1] = 10;
                // fre
                if(bit_fre) // kHz
                {
                    SegBuf[2] = ( (Fre >= 10000000) ? (Fre /10000000 % 10) : 10 );
                    SegBuf[3] = ( (Fre >= 1000000) ? (Fre /1000000 % 10) : 10 );
                    SegBuf[4] = ( (Fre >= 100000) ? (Fre /100000 % 10) : 10 );
                    SegBuf[5] = ( (Fre >= 10000) ? (Fre /10000 % 10) : 10 );
                    SegBuf[6] = ( (Fre >= 1000) ? (Fre /1000 % 10) :0)+15;
                    SegBuf[7] = Fre/100 % 10;
                } 
                else
                {
                    SegBuf[2] = ( (Fre >= 100000) ? (Fre /100000 % 10) : 10 );
                    SegBuf[3] = ( (Fre >= 10000) ? (Fre /10000 % 10) : 10 );
                    SegBuf[4] = ( (Fre >= 1000) ? (Fre /1000 % 10) : 10 );
                    SegBuf[5] = ( (Fre >= 100) ? (Fre /100 % 10) : 10 );
                    SegBuf[6] = ( (Fre >= 10) ? (Fre /10 % 10) : 10 );
                    SegBuf[7] = Fre % 10;
                }
              
                break;

               
                case 1: // 湿度
                    SegBuf[0] = 12;
                    SegBuf[1] = 10;
                    SegBuf[2] = 10;
                    SegBuf[3] = 10;
                    SegBuf[4] = 10;
                    SegBuf[5] = 10;
                //
                    SegBuf[6] = ( (ADC_Wet>=10)?(ADC_Wet / 10 % 10):10 );
                    SegBuf[7] = ADC_Wet % 10;
                break;
                
                case 2: // dis
                    SegBuf[0] = 13;
                    SegBuf[1] = 10;
                    SegBuf[2] = 10;
                    SegBuf[3] = 10;
                    SegBuf[4] = 10;
                // 距离显示
                if(bit_dis)
                {
                    SegBuf[5] = ( (distance >= 100)? (distance / 100 % 10): (0) )+15;
                    SegBuf[6] = ( (distance >= 10)? (distance / 10 % 10): (0) );
                    SegBuf[7] = distance % 10;
                }
                else
                {
                    SegBuf[5] = ( (distance >= 100)? (distance / 100 % 10): (10) );
                    SegBuf[6] = ( (distance >= 10)? (distance / 10 % 10): (10) );
                    SegBuf[7] = distance % 10;
                }
                break;
                
                case 3: // set
                    SegBuf[0] = 14;
                    SegBuf[1] = Interface_p+1;
                    SegBuf[2] = 10;
                    SegBuf[3] = 10;
                    SegBuf[4] = 10;
                    if(Interface_p == 0)
                    {
                        SegBuf[5] = ( (P_fre >= 100)? (P_fre/100%10):10 );
                        SegBuf[6] = P_fre/10%10+15;
                        SegBuf[7] = P_fre%10;
                    }
                    else if(Interface_p == 1)
                    {
                        SegBuf[5] = 10;
                        SegBuf[6] = P_wet/10%10;
                        SegBuf[7] = 0;
                    }
                    else if(Interface_p == 2)
                    {
                        SegBuf[5] = 10;
                        SegBuf[6] = P_dis/10%10+15;
                        SegBuf[7] = P_dis%10;
                    }
                break;
            }
            
        }
        
        
        if(Timer_300ms)
        {
            Timer_300ms = 0;
                EA = 0;
                IIC_Start();
                IIC_SendByte(0x90);
                IIC_WaitAck();
                IIC_SendByte(0x03);
                IIC_WaitAck();
                EA = 1;
                IIC_Start();
                IIC_SendByte(0x91);
                IIC_WaitAck();
                IIC_RecByte();
                IIC_SendAck(1);
                IIC_Stop();
            // ADC_PCF8591(0x03);
            delay_ms(16);
            
                IIC_Start();
                IIC_SendByte(0x90);
                IIC_WaitAck();
                IIC_SendByte(0x03);
                IIC_WaitAck();
                EA = 0;
                IIC_Start();
                IIC_SendByte(0x91);
                IIC_WaitAck();
                ADC_Wet = IIC_RecByte();
                IIC_SendAck(1);
                IIC_Stop();
                EA = 1;
            // ADC_Wet = ADC_PCF8591(0x03);
            ADC_Wet = ADC_Wet * 20.0 / 51.0; // wet 【计算湿度】
            
            if(ADC_Wet >= 99) ADC_Wet =99;
            
            // 计算DAC输出功能
            if(ADC_Wet <= 20)
            {
                DAC_out = 51;
            }
            else if(ADC_Wet >= 80)
            {
                DAC_out = 255;
            }
            else
            {
                // 图15 少了东西，脑补输出电压为1时对应的湿度为20%RH
                DAC_out = 51.0*ADC_Wet/15.0 - 51.0/3.0;
            }
            
            // 两次，还是为了稳妥
            EA = 0;
            DAC_PCF8591(DAC_out);
            EA = 1;
            delay_ms(16);
            EA = 0;
            DAC_PCF8591(DAC_out);
            EA = 1;
            
        }
        
        
        if(Timer_500ms)
        {
            Timer_500ms = 0;
            // 超声波测距
            csb_GetDis();
            
            // 继电器控制功能
            // 用标志位是因为与脉冲输出功能冲突
            if(distance/10 > P_dis) // 当前测量到的距离结果大于距离参数
            {
                if(dis_flag == 0)//如果继电器是未吸合的
                {
                    dis_flag = 1;// 继电器吸合
                    Relay_cnt ++;// 继电器开关次数加一
                    EA = 0;
                    EEPROM_Write(0x00, Relay_cnt);  // 存到EEPROM中
                    EA = 1;
                }
            }
            else//否则继电器断开
            {
                dis_flag = 0;
            }
            
            // 脉冲输出功能的占空比控制
            if(Fre/100 > P_fre) // 频率大于频率参数
            {
                Wave_duty = 4;
                //  4/5，前四个0.2ms（也就是0.8ms）高电平，占空比就是80%
            }
            else
            {
                Wave_duty = 1;
                //  1/5，前一个0.2ms（也就是0.2ms）高电平，占空比就是20%
            }
        }
        
        
        if(Timer_50ms)
        {
            Timer_50ms = 0;
            
            // LED 状态指示
            if(Interface_main != 3) // 0、1、2界面，对应L1、L2、L3
                                    // 0x01 << 0 ,就是L1
                LEDstate = ~(0x01 << Interface_main);
            else if(Interface_main == 3)    // 参数界面需要LED闪烁
            {
                // 100ms计时器的妙用
                if(SysTimer_led % 2==0)     // 计时器是偶数
                {
                    LEDstate = ~(0x01 << Interface_p); // 灯亮
                }
                else// 计时器是奇数
                    LEDstate = 0xFF;    //灯灭
            }
            
            if(Wave_duty == 4)  // 若当前测量到的频率数据大于频率参数，指示灯 L4 点亮
            {
                LEDstate &= 0xF7;
            }
            if(ADC_Wet > P_wet) // 若当前测量到的湿度数据大于湿度参数，指示灯 L5 点亮
            {
                LEDstate &= 0xEF;
            }
            if(dis_flag)        // 若当前测量到的距离数据大于距离参数，指示灯 L6 点亮
            {
                LEDstate &= 0xDF;
            }
            
            // 控制外设之类的，个人不喜欢封装函数（因为我觉得调用函数需要时间，效率不高）
                EA = 0;
            P0 = LEDstate;
            P2 = (P2 & 0x1F) | 0x80;
            P2 &= 0x1F;
                EA = 1;
            
            delay_ms(8);//延时是怕关了中断有影响，延时一段时间刷新数码管
        }
        
        
    }
}

void delay_ms(unsigned int ms)
{
    unsigned int i;
    while (ms--)
        for (i = 640; i > 0; i--);
}


void scb_Init(void) 
{
	unsigned char i;
    EA = 0;
	for(i=0; i<8;i++)
	{
		csb_TX = 1;
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 1
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 2
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 3
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 4
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 5
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 6
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 7
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 8
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 9
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 10
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 11
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 12
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 13
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 14
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 0.7
        csb_TX = 0;
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 1
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 2
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 3
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 4
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 5
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 6
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 7
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 8
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 9
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 10
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 11
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 12
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();  // 13
		_nop_();_nop_();_nop_();  // 0.3 
	}
    AUXR |= 0x10;		//定时器2开始计时
    EA = 1;
}

void Timer2Init_csb(void)		//100微秒@12.000MHz
{
	AUXR &= 0xFB;		//定时器时钟12T模式
	T2L = 0;		//设置定时初始值
	T2H = 0;		//设置定时初始值
	//AUXR |= 0x10;		//定时器2开始计时
    IE2 |= 0x04;// 
}

void csb_GetDis(void)
{
	unsigned int time;

	T2L = 0;		//设置定时初始值
	T2H = 0;		//设置定时初始值
	scb_Init();
	
	while ((csb_RX == 1) && (csb_error == 0));		  
	AUXR &= 0xEF;		//定时器2 STOP
    
	if (csb_error == 1) 
	{
		csb_error = 0;
		distance = 999;
	}
	else
	{
        time = (T2H<<8) + T2L;
		distance = (unsigned int)time * 0.017;
	}
}

void t2_isr() interrupt 12
{
    if(csb_error == 0)
        csb_error = 1;
}
