#ifndef __TIMER__
#define __TIMER__

#include "stc15f2k60s2.h"

extern unsigned int SysTimer;
extern unsigned int SysTimer_led;
extern bit Timer_10ms;
extern bit Timer_300ms;
extern bit Timer_500ms;
extern bit Timer_50ms;

extern bit dis_flag;

extern unsigned char Wave_duty;
extern unsigned int Fre;

extern unsigned char SegBuf[8];

void SegRefresh(void);
void Timer1Init(void);
void Timer0Init_FRE(void);

unsigned char BTN_KeyScan(void);

#endif